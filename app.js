const axios = require("axios");
const fs = require("fs");

// thêm số 0 vào sđtnpma
// lấy tiếng đầu firstName
// sđt null = số ph
// lọc dấu phẩy

const axiosClient = axios.create({
    baseURL: "https://quanlyapi.tamchitai.edu.vn",
    //timeout: 5000, // default is 0 (no timeout)
    // responseType: 'json'
});

axiosClient.interceptors.request.use(async (config) => {
    // Handle token here ...
    //check token
    const token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkdWN0aGFuZzEiLCJleHAiOjE2NTczODk4NjN9.27ZurvNHXK7qZSh8KalaxedRIgCocP31_amBV1e0IJbU9tkRtTW_DIsO6kSAMdq3rG9JONivS2CZg6n1bCP3tA";
    config.headers.Authorization = `Bearer ${token}`;
    return config;
});

axiosClient.interceptors.response.use((response) => {
   
    if (response && response.data !== undefined) {
        // only get data
        return response.data;
    }

    return response;
}, async (error) => {
    // handle token expried
    // auto redirect to sign in page

    // Handle errors
    if (error.response.data === "expried token") {
        await localStorage.clear();
        await sessionStorage.clear();
        window.location.href = "/";
        await alert("Phiên làm việc của bạn đã hết hạn! vui lòng đăng nhập lại")
        throw error.response.data;
    } else if(error.request){
        throw error.request
    }else{
        throw error;
    }
});

let data = fs.readFileSync("./lop12.json", { encoding: 'utf-8', flag: 'r' });
let students = JSON.parse(data);
var totalDeactiveStudent = 0;

async function addStudentToGrade(student,grade) {

    if (student.d === "x" && student.h === "x"){
        totalDeactiveStudent++;
        return null;
    }
   
    if (student.studentnumber && student.studentnumber[0] !== '0') student.studentnumber = "0" + removeColon(student.studentnumber);
    if (student.parennumber && student.parennumber[0] !== '0') student.parennumber = "0" + removeColon(student.parennumber);
  
  
    const firstNameArr = student.firstname.split(" ");
    const body = {
        "studentNumber": student.studentnumber || student.parennumber,
        "userName": student.studentnumber || student.parennumber,
        "password": genPass(firstNameArr[0], student.lastname),
        "school": firstNameArr[1],
        "grade": grade,
        "firstName": firstNameArr[0].replace(/\s+/g, ' ').trim(),
        "lastName": student.lastname.replace(/\s+/g, ' ').trim(),
        "role": "STUDENT",
        "status": "active",
        "social": (student.facebooklink) ? student.facebooklink : null,
        "parentNumber": student.parennumber,
        "parentName": (student.parentname) ? student.parentname.replace(/\s+/g, ' ').trim() : "",
        "note":student.note
    }
   
    try {

            const studentId = await axiosClient.post('/api/v1/students/', body);
            const bodyToAddClasses = [];
            if (student.d !== "c" && student.d !== "x" && student.d !== "s" && student.d !== "bảo lưu" && student.d !== undefined){ // khác lớp ct và không học
                bodyToAddClasses.push({
                    studentId: studentId,
                    classId: student.d // lớp đại
                })
            }
            if (student.h !== "c" && student.h !== "x" && student.h !== "s" && student.h !== "bảo lưu" && student.h !== undefined){
                bodyToAddClasses.push({
                    studentId: studentId,
                    classId: student.h // lớp hình
                })
            }
            const addStudentToClasses = await axiosClient.post(`/api/v1/students/${studentId}/classes`,bodyToAddClasses);
            if(addStudentToClasses === "create successful!"){
                
            }
        
        
    } catch (error) {
        console.log( student.lastname.replace(/\s+/g, ' ').trim() + " " +  firstNameArr[0].replace(/\s+/g, ' ').trim() );
        

    }
};

async function main() {
    console.log(students.length);
    for (var i = 0; i < students.length; i++) {
        // if(students[i].d === "n" && students[i].h === "n"){
        //     // console.log(students[i]);
        //     continue;
        // }
        // if( students[i].studentnumber !== undefined && students[i].parennumber !== undefined){
        //     // console.log(students[i]);
        //     continue;
        // }
        await addStudentToGrade(students[i],12);
        
    }
    console.log(totalDeactiveStudent);
}

const genPass = (firstName, lastName) => {
    firstName = firstName.replace(/\s+/g, ''); // xóa khoảng trắng
    firstName = removeAccents(firstName);
    firstName = capitalizeFirstLetter(firstName);

    lastName = lastName.replace(/\s+/g, ''); // xóa khoảng trắng
    lastName = removeAccents(lastName);
    lastName = capitalizeFirstLetter(lastName);

    return lastName + firstName;
}

const removeAccents = (str) => {
    return str.normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd').replace(/Đ/g, 'D');
}

const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const removeColon = (string) => {
    try {
        return string.replaceAll(",", '').replaceAll(".", '');
    } catch (error) {
        return string;
    }
    
}

main();